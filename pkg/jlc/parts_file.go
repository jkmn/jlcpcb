package jlc

import (
	"encoding/csv"
	"log"
	"os"
	"strconv"
	"strings"

	"github.com/tealeg/xlsx/v3"

	"gitlab.com/jkmn/jlcpcb/pkg/util"
)

func ParsePartsFile(path string) map[string]Part {
	var rows [][]string

	if strings.HasSuffix(path, "xlsx") {
		wb, err := xlsx.OpenFile(path)
		if err != nil {
			panic(err)
		}

		if len(wb.Sheets) == 0 {
			log.Fatal("The parts file contains no sheets.")
		}
		xlsxSheet := wb.Sheets[0]

		rows = util.SimplifSheet(xlsxSheet)
	}

	if strings.HasSuffix(path, "csv") {
		f, err := os.Open(path)
		if err != nil {
			log.Fatal(err)
		}
		defer f.Close()

		rdr := csv.NewReader(f)

		rows, err = rdr.ReadAll()
		if err != nil {
			log.Fatal(err)
		}
	}

	parts := make(map[string]Part)

	var headerMap map[string]int
	for i := 0; i < len(rows); i++ {
		row := rows[i]
		if i == 0 {
			headerMap = parsePartsHeaders(row)
		} else {
			part := parsePartsRow(row, headerMap)
			if part == nil {
				log.Printf("invalid part on row %d", i+1)
				return parts
			}
			parts[part.LcscPart] = *part
		}
	}

	return parts
}

func parsePartsRow(row []string, headerMap map[string]int) *Part {
	part := &Part{}

	for header, cellIdx := range headerMap {
		if cellIdx >= len(row){
			continue
		}

		v := row[cellIdx]


		switch header {
		case ColLCSCPart:
			part.LcscPart = v
		case ColFirstCategory:
			part.FirstCategory = v
		case ColSecondCategory:
			part.SecondCategory = v
		case ColMfRPart:
			part.MfrPart = v
		case ColPackage:
			part.Package = util.SimplifyFootprint(v)
		case ColSolderJoint:
			part.SolderJoint, _ = strconv.Atoi(v)
		case ColManufacturer:
			part.Manufacturer = v
		case ColLibraryType:
			part.LibraryType = v
		case ColDescription:
			part.Description = v
		case ColDatasheet:
			part.Datasheet = v
		case ColPrice:
			part.Price = parsePriceBreaks(v)
		case ColStock:
			part.Stock, _ = strconv.Atoi(v)
		}
	}

	if part.LcscPart == "" {
		return nil
	}

	return part
}

func parsePriceBreaks(s string) map[int]float64 {
	if s == "" {
		return nil
	}

	out := make(map[int]float64)

	breaks := strings.Split(s, ",")

	for _, b := range breaks {
		chunks := strings.Split(b, ":")

		qty_ := strings.Split(chunks[0], "-")[0]
		qty, err := strconv.Atoi(qty_)
		if err != nil {
			log.Fatal(err)
		}

		price, err := strconv.ParseFloat(chunks[1], 64)
		if err != nil {
			log.Fatal(err)
		}

		out[qty] = price
	}

	return out
}

func parsePartsHeaders(row []string) map[string]int {
	headerMap := make(map[string]int)
	for i := 0; i < len(row); i++ {
		s := row[i]
		switch s {
		case ColLCSCPart:
			headerMap[ColLCSCPart] = i
		case ColFirstCategory:
			headerMap[ColFirstCategory] = i
		case ColSecondCategory:
			headerMap[ColSecondCategory] = i
		case ColMfRPart:
			headerMap[ColMfRPart] = i
		case ColPackage:
			headerMap[ColPackage] = i
		case ColSolderJoint:
			headerMap[ColSolderJoint] = i
		case ColManufacturer:
			headerMap[ColManufacturer] = i
		case ColLibraryType:
			headerMap[ColLibraryType] = i
		case ColDescription:
			headerMap[ColDescription] = i
		case ColDatasheet:
			headerMap[ColDatasheet] = i
		case ColPrice:
			headerMap[ColPrice] = i
		case ColStock:
			headerMap[ColStock] = i
		case "":
			return headerMap
		}
	}
	return headerMap
}

const (
	ColLCSCPart       = "LCSC Part"
	ColFirstCategory  = "First Category"
	ColSecondCategory = "Second Category"
	ColMfRPart        = "MFR.Part"
	ColPackage        = "Package"
	ColSolderJoint    = "Solder Joint"
	ColManufacturer   = "Manufacturer"
	ColLibraryType    = "Library Type"
	ColDescription    = "Description"
	ColDatasheet      = "Datasheet"
	ColPrice          = "Price"
	ColStock          = "Stock"
)
