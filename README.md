This is a repo for generally useful things related to JLCPCB.

At the time of writing, there are two tools that are working and useful: `jlcfind` and `jlcbomcheck`.

`jlcfind` performs a basic search through the JCL SMT parts catalog. IMHO, it's *much* faster than trying to use the online catalog search. You supply the search terms on the command line, and it returns the result on STDOUT formatted as JSON.

`jlcbomcheck` compares a BOM file with the JLC catalog to tell you about various things that might be interesting to you. For example:

```
These parts do not have a LCSC part number:

C420        750p        Unpolarized capacitor
C421        130p        Unpolarized capacitor
D101,D102   D_Schottky  Schottky diode


These parts are from the extended library:

C212,C216                       150n        Unpolarized capacitor
C422                            390p        Unpolarized capacitor
D102,D201,D204,D205,D206,D207,D208,D301,D401,D402    BAT54S        Dual Schottky diode, anode/cathode/center
TH401                           10k_NTC     Temperature dependent resistor, negative temperature coefficient, US symbol
U202,U304,U305                  LM4562      Dual High-Performance, High-Fidelity Audio Operational Amplifier, DIP-8/SOIC-8/TO-99-8
U301                            74CBTLV3253
U302                            74ACT00     quad 2-input NAND gate


These footprints of these parts do not match:

D102,D201,D204,D205,D206,D207,D208,D301,D401,D402    (SOT-23 vs SOT-23-3)   BAT54S        Dual Schottky diode, anode/cathode/center
L401                            (Toroid_Vertical_L24.6mm_W15.5mm_P11.44mm_Pulse_KM-4 vs 0805)    1u        Inductor
Q401,Q402,Q403,Q404             (SOT-23 vs SOT-23-3)                        BS170F        0.15A Id, 60V Vds, N-Channel MOSFET, SOT-23
U101                            (SOT-23-6 vs SOT-23-3L)                     AP6320x
U301                            (SOIC-16 vs SSOP-16)                        74CBTLV3253
```