package main

import (
	"encoding/json"
	"flag"
	"log"
	"os"
	"sort"
	"strings"

	"gitlab.com/jkmn/jlcpcb/pkg/jlc"
)

var (
	partsPath *string = flag.String("parts", "", "JLC parts file")
)

func main() {
	flag.Parse()

	if *partsPath == "" {
		flag.Usage()
		log.Print("Please provide the path to the JLC parts file to reference.")
		log.Print("The JLC parts file can be found at [https://jlcpcb.com/parts].")
		os.Exit(1)
	}

	terms := flag.Args()

	potentials := make([]jlc.Part, 0)
	parts := jlc.ParsePartsFile(*partsPath)
	terms = sanitizeTerms(terms)

	for _, part := range parts {
		tests := []string{
			part.LcscPart,
			part.FirstCategory,
			part.SecondCategory,
			part.MfrPart,
			part.Package,
			part.Manufacturer,
			part.LibraryType,
			part.Description,
		}
		test := strings.Join(tests, "\n")
		test = strings.ToLower(test)

		isPotential := true
		for _, term := range terms {
			if !strings.Contains(test, term) {
				isPotential = false
			}
		}
		if isPotential {
			potentials = append(potentials, part)
		}
	}

	sort.Sort(PartsByPrice(potentials))

	enc := json.NewEncoder(os.Stdout)
	enc.SetIndent("", "\t")
	err := enc.Encode(potentials)
	if err != nil {
		log.Fatal(err)
	}

	log.Print("Result quantity: ", len(potentials))

}

func sanitizeTerms(terms []string) []string {
	out := make([]string, 0)
	for _, term := range terms {
		term = strings.TrimSpace(term)
		term = strings.ToLower(term)
		if term != "" {
			out = append(out, term)
		}
	}

	return out
}

type PartsByPrice []jlc.Part

func (p PartsByPrice) Len() int {
	return len(p)
}

func (p PartsByPrice) Less(i, j int) bool {
	pi := p[i].LowQtyPrice()
	p2 := p[j].LowQtyPrice()
	return pi < p2
}

func (p PartsByPrice) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}
