package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
	"text/tabwriter"

	"gitlab.com/jkmn/jlcpcb/pkg/jlc"
)

var (
	bomPath      *string = flag.String("bom", "", "BOM to process")
	partsPath    *string = flag.String("parts", "", "JLC parts file")
	showExtended *bool   = flag.Bool("ext", false, "show extended parts")
)

func main() {
	flag.Parse()
	if *bomPath == "" {
		flag.Usage()
		log.Print("Please provide the path to a BOM file to process.")
		os.Exit(1)
	}

	if *partsPath == "" {
		flag.Usage()
		log.Print("Please provide the path to the JLC parts file to reference.")
		log.Print("The JLC parts file can be found at [https://jlcpcb.com/parts].")
		os.Exit(1)
	}

	parts := jlc.ParsePartsFile(*partsPath)

	mats := parseBOM()

	extended := make([]Pair, 0)
	noJlcPart := make([]Pair, 0)
	badJlcPart := make([]Pair, 0)
	badFootprint := make([]Pair, 0)

	for _, mat := range mats {
		pair := Pair{
			Material: mat,
		}

		if mat.LcscPartNo == "" {
			noJlcPart = append(noJlcPart, pair)
			continue
		}

		part, ok := parts[mat.LcscPartNo]
		if !ok {
			badJlcPart = append(badJlcPart, pair)
			continue
		}

		pair.Part = part

		if *showExtended && part.LibraryType == "Extended" {
			extended = append(extended, pair)
		}

		if part.Package != mat.Footprint {
			badFootprint = append(badFootprint, pair)
		}
	}

	// initialize tabwriter
	w := new(tabwriter.Writer)

	// minwidth, tabwidth, padding, padchar, flags
	w.Init(os.Stdout, 8, 8, 0, '\t', 0)


	if len(noJlcPart)>0{
		fmt.Print("\n\nThese parts do not have a LCSC part number:\n\n")
		for _, pair := range noJlcPart {
			refs := strings.Join(pair.References, ",")
			fmt.Fprintf(w,"%s\t%s\t%s\n", refs, pair.Value, pair.Material.Description)
		}

		w.Flush()
	}

	if len(badJlcPart)>0{
		fmt.Print("\n\nThese parts do not have a valid LCSC part number:\n\n")
		for _, pair := range badJlcPart {
			refs := strings.Join(pair.References, ",")
			fmt.Fprintf(w,"%s\t%s\t%s\n", refs, pair.Value, pair.Material.Description)
		}

		w.Flush()
	}

	if len(extended)>0{
		fmt.Print("\n\nThese parts are from the extended library:\n\n")
		for _, pair := range extended {
			refs := strings.Join(pair.References, ",")
			fmt.Fprintf(w,"%s\t%s\t%s\n", refs, pair.Value, pair.Material.Description)
		}

		w.Flush()
	}

	if len(badFootprint)>0{
		fmt.Print("\n\nThese footprints of these parts do not match:\n\n")
		for _, pair := range badFootprint {
			refs := strings.Join(pair.References, ",")
			fmt.Fprintf(w,"%s\t(%s vs %s)\t%s\t%s\n", refs, pair.Material.Footprint, pair.Part.Package, pair.Value, pair.Material.Description)
		}

		w.Flush()
	}

}

type Pair struct {
	jlc.Part
	Material
}
