jlcbomcheck:
	go build ./cmd/jlcbomcheck/

jlcfind:
	go build ./cmd/jlcfind/

partcards:
	go build ./cmd/partcards/

BINS=jlcbomcheck jlcfind partcards

.PHONY: $(BINS)
